使用vuepress

[TOC]

# 1 快速上手

::: warning 前提条件
VuePress 需要 [Node.js](https://nodejs.org/en/) >= 8.6
:::

本文会帮助你从头搭建一个简单的 VuePress 文档。如果你想在一个现有项目中使用 VuePress 管理文档，从步骤 3 开始。

1. 创建并进入一个新目录

   ``` bash
   mkdir vuepress-starter && cd vuepress-starter
   ```

2. 使用你喜欢的包管理器进行初始化

   ``` bash
   yarn init # npm init
   ```

3. 将 VuePress 安装为本地依赖

   我们已经不再推荐全局安装 VuePress

   ``` bash
   yarn add -D vuepress # npm install -D vuepress
   ```

   ::: warning 注意
   如果你的现有项目依赖了 webpack 3.x，我们推荐使用 [Yarn](https://classic.yarnpkg.com/zh-Hans/) 而不是 npm 来安装 VuePress。因为在这种情形下，npm 会生成错误的依赖树。
   :::

4. 创建你的第一篇文档

   ``` bash
   mkdir docs && echo '# Hello VuePress' > docs/README.md
   ```

5. 在 `package.json` 中添加一些 [scripts](https://classic.yarnpkg.com/zh-Hans/docs/package-json#toc-scripts)

   这一步骤是可选的，但我们推荐你完成它。在下文中，我们会默认这些 scripts 已经被添加。

   ``` json
   {
     "scripts": {
       "docs:dev": "vuepress dev docs",
       "docs:build": "vuepress build docs"
     }
   }
   ```

6. 在本地启动服务器

   ``` bash
   yarn docs:dev # npm run docs:dev
   ```

   VuePress 会在 [http://localhost:8080](http://localhost:8080) 启动一个热重载的开发服务器。

现在，你应该已经有了一个简单可用的 VuePress 文档。接下来，了解一下推荐的 [目录结构](directory-structure.html) 和 VuePress 中的 [基本配置](basic-config.html)。

等你了解完上文介绍的基础概念，再去学习一下如何使用 [静态资源](assets.html)，[Markdown 拓展](markdown.html) 和 [在 Markdown 中使用 Vue](using-vue.html) 来丰富你的文档内容。

当你的文档逐渐成型的时候，不要忘记 VuePress 的 [多语言支持](i18n.html) 并了解一下如何将你的文档 [部署](deploy.html) 到任意静态文件服务器上。

# 2 详细使用

## 2.1 安装下载

**（1）安装nodesjs**

[nodejs 下载](https://links.jianshu.com/go?to=https%3A%2F%2Fnodejs.org%2Fen%2F)，建议安装 nodejs 17 之前的版本，亲测使用 nodejs 18 会有错误：`digital envelope routines::unsupported`。（2023-03-10）

**（2）安装 vuepress**

官方不再推荐全局安装，但是我把 vuepress 安装为本地依赖总是有问题，最终还是安装全局依赖。

打开终端，运行以下命令即可安装：

```shell
npm install -g vuepress
```

## 2.2 目录结构

创建目录，并在目录打开终端对目录进行初始化：

```
npm init -y
```

推荐的目录结构如下：

```java
.
├── docs/
│   ├── .vuepress/
|   |   |
│   │   ├── public/
│   │   ├── config.js
│   │
│   │
│   ├── README.md
│
└── package.json
```

其中：

`docs/.vuepress`: 用于存放全局的配置、组件、静态资源等。
 `docs/.vuepress/public`: 静态资源目录。。
 `docs/.vuepress/config.js`: 配置文件的入口文件，也可以是 YML 或 toml。

修改 `pakage.json` 内容：

```json
{
  "name": "gptbook",
  "version": "1.0.0",
  "description": "GPT Book",
  "main": "index.js",
  "scripts": {
    "docs:dev": "vuepress dev docs",
    "docs:build": "vuepress build docs"
  },
  "repository": {
    "type": "git",
    "url": "https://gitee.com/aisets/gpt-book.git"
  },
  "keywords": [
    "GPT",
    "ChatGPT"
  ],
  "author": "RunAtWorld",
  "license": "Apache 2.0",
  "devDependencies": {
    "vuepress": "^1.5.4"
  }
}

```

**页面路由**

|    文件相对路径     |      页面路由地址      |
| :-----------------: | :--------------------: |
|    `/README.md`     |          `/`           |
|  `C语言/README.md`  |       `/C语言/`        |
| `C语言/基础知识.md` | `/C语言/基础知识.html` |

所以主页面的内容就写在 `docs/README.md`:


```md
---
home: true
heroImage: /favicon.ico
actionText: open
actionLink: /guide/
lang: zh-CN
---

\`\`\`c {5}
int main(int argc, char *argv[])
{
    while(!is_death(me)) {
        reading();
        studying();
        coding();
        sharing();
    }

    return EXIT_SUCCESS;
}
\`\`\`

::: slot footer
MIT Licensed | Copyright © 2023 [shachi](https://chunni98.github.io)
:::
```

## 2.3 配置

`config.js` 内是全局配置：

```js
module.exports = {
    title: 'ChatGPT教程',
    description: 'Book about ChatGPT',
    base : '/gpt-book/',
    markdown: {
      lineNumbers: true
    },
    themeConfig: {
        sidebarDepth: 2,
        // logo: '/assets/img/logo.png',
        nav: [
          {
            text: 'ChatGPT',
            ariaLabel: 'ChatGPT',
            items: [
              { text: 'ChatGPT注册', link: '/ChatGPT/ChatGPT注册/' },
              { text: '国内使用ChatGPT', link: '/ChatGPT/国内使用ChatGPT/' },
            ]
          },
          { text: 'Midjourney', link: '/Midjourney/' },
          {
            text: '每日论文',
            ariaLabel: '每日论文',
            items: [
              { text: '大语言模型综述', link: '/paper_everyday/LLM_Survey' },
              { text: '大语言模型实践指南', link: '/paper_everyday/LLM_Practice' },
            ]
          }
        ],
        sidebar: {
          '/': [
            '',    
            '/ChatGPT/',
            '/Midjourney/',
          ],
          '/ChatGPT/': [
            '',  
            'ChatGPT注册',
            '国内使用ChatGPT',
          ],
          '/Midjourney/': [
            '',  
            '国内使用Midjourney',
          ]
        },
        lastUpdated: 'Last Updated', // string | boolean
      }
  }
```
## 2.4 运行和编译

在目录下打开终端：

```shell
# 测试运行输入这个命令
yarn docs:dev

# 编译输入这个命令
yarn docs:build
```

运行后可在 `localhost:8080` 预览，vuepress 支持热重载。

运行命令可能出现错误：`Error: Cannot find module ‘vue-template-compiler’`

在目录下运行命令: `npm install vue-template-compiler` 安装缺失的包即可。


----

参考

1. [vuepress官方博客](https://vuepress.vuejs.org/zh)
2. [1小时搞定vuepress快速制作vue文档/博客+免费部署预览](https://juejin.cn/post/6844903999129436174)