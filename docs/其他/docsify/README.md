# docsify

> 一个神奇的文档网站生成器。

[docsify官方文档](https://docsify.js.org/) / [docsify官方Github](https://github.com/docsifyjs)

## 概述

docsify 可以快速帮你生成文档网站。不同于 GitBook、Hexo 的地方是它不会生成静态的 `.html` 文件，所有转换工作都是在运行时。如果你想要开始使用它，只需要创建一个 `index.html` 就可以开始编写文档并直接[部署在 GitHub Pages](zh-cn/deploy.md)。

查看[快速开始](zh-cn/quickstart.md)了解详情。

### 特性

- 无需构建，写完文档直接发布
- 容易使用并且轻量 (压缩后 ~21kB)
- 智能的全文搜索
- 提供多套主题
- 丰富的 API
- 支持 Emoji
- 兼容 IE11
- 支持服务端渲染 SSR ([示例](https://github.com/docsifyjs/docsify-ssr-demo))

查看 [Showcase](https://github.com/docsifyjs/docsify/#showcase) 了解更多在使用 docsify 的文档项目。


## 社区

在 [Discord](https://discord.gg/3NwKFyR) 的社区里可以找到 docsify 的用户和开发者团队。


## 首页html设置

首页 index.html 参考下面

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <link rel="icon" type="image/x-ico" href="_media/icon.ico">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="description" content="Description">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/docsify/lib/themes/vue.css">
  <script src="//cdn.jsdelivr.net/npm/docsify/lib/plugins/disqus.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/docsify-pagination/dist/docsify-pagination.min.js"></script>
</head>
<body>
  <div id="app"></div>
  <script>
    window.$docsify = {
      el: '#app',
      name: '成长',
      repo: ',
      loadSidebar: true,
      subMaxLevel: 2,
      loadNavbar: true,
      coverpage: true,
      auto2top: true,
      disqus: 'shortname',
      nameLink: '/',
      count:{
        countable:true,
        fontsize:'0.9em',
        color:'rgb(90,90,90)',
        language:'chinese'
      },
      search: {
        placeholder: '搜索',
        noData: '找不到结果!'
      }
    }
  </script>
  <script src="//cdn.jsdelivr.net/npm/docsify/lib/plugins/search.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/docsify/lib/docsify.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/docsify/lib/plugins/emoji.min.js"></script>
  <script src="//unpkg.com/docsify-count/dist/countable.js"></script>
</body>
</html>
```

## FAQ

1. 如果由于CDN失效导致网站访问较慢或无法访问，可以使用本仓库下的[js文件](https://gitee.com/sifangcloud/web_ui_notes/tree/master/docs/%E5%85%B6%E4%BB%96/docsify/js/docsify/4.12.1) 。 或者换其他的CDN，备选CDN有
   - https://www.bootcdn.cn/docsify/ (支持国内)
   - https://cdn.jsdelivr.net/npm/docsify/ (国内外都支持)
   - https://cdnjs.com/libraries/docsify
   - https://unpkg.com/browse/docsify/
