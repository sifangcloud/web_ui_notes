#!/bin/bash
file_dir=$1

echo "get repo name:"${file_dir}
cd /app/${file_dir}

# hexo
hexo_repos=(pyfuns)
if echo "${hexo_repos[@]}" | grep -w "${file_dir}" &>/dev/null; then
    echo "update repo:"${file_dir}
    git checkout hexo && git pull -f origin hexo:hexo
fi


# docsify
docsify_repos=(qt_docs LeetcodeEveryday CppPath)
if echo "${docsify_repos[@]}" | grep -w "${file_dir}" &>/dev/null; then
    echo "update repo:"${file_dir}
    git pull -f origin master:master
fi

# vuepress
vuepress_repos=(InterviewNotes)
if echo "${vuepress_repos[@]}" | grep -w "${file_dir}" &>/dev/null; then
    echo "update repo:"${file_dir}
    git pull -f origin master:master
    yarn docs:build
fi

# gitbook
gitbook_repos=(devbook web_ui_notes JavaPrinciple bigdata_path LinuxFun pydemo ai_math aipath k8spath)
if echo "${gitbook_repos[@]}" | grep -w "${file_dir}" &>/dev/null; then
    echo "update repo:"${file_dir}
    git pull -f origin master:master
    gitbook install
    gitbook build
fi

echo "succeed to update:"${file_dir}